const fontSizeRelative = 16,
	$xs = 0,
	$sm = 576,
	$md = 768,
	$lg = 992,
	$xl = 1200,
	$xxl = 1400,
	$xxxl = 1920;

	
module.exports = [
	{
		name: 'media',
		options: [
			{ '$xs': 0 },
			{ '$sm': 576 },
			{ '$md': 768 },
			{ '$lg': 992 },
			{ '$xl': 1200 },
			{ '$xxl': 1400 },
			{ '$xxxl': 1920 },
		]
	},
	{
		name: 'global',
		options: [
			{'$font': '\'Proxima Nova\''},
			{'$color': '#828282'},
			{'$linkColor': '#4F4F4F'},
			{'$backgroundColor': '#F7F7F7'},
			{'$borderColor': '#CECECE'},
			{'$fontSizeRelative': fontSizeRelative},
			{'$fontSize': '1rem'},
			{'$isBootstrap': true},
		]
	},
	{
		name: 'optionsConfig',
		options: [
			{
				name: 'grid',
				selector: "container",
				options: [
					{
						selector: '',
						value: '1440px',
						gutters: ''
					},
				]
			},
			{
				name: 'font-family',
				selector: "ff",
				options: [
					{
						name: 'Proxima Nova',
						src: '../fonts/proxima-nova/proxima-nova',
						options: [
							{
								weight: 100,
								style: 'normal'
							},
							{
								weight: 300,
								style: 'normal'
							},
							{
								weight: 400,
								style: 'normal'
							},
							{
								weight: 700,
								style: 'normal'
							},
							{
								weight: 800,
								style: 'normal'
							},
							{
								weight: 900,
								style: 'normal'
							},
						]
					}
				]
			},
			{
				name: 'font-size-root',
				selector: "fzr",
				options: [
					{
						mediaString: 'xs',
						media: $xs,
						value: '13px'
					},
					{
						mediaString: 'md',
						media: $md,
						value: '15px'
					},
					{
						mediaString: 'xxl',
						media: $xxl,
						value: '16px'
					},
				]
			},
			{
				name: 'font-size-title',
				selector: "h",
				options: [
					{ '1': 60 / fontSizeRelative + 'rem' },
					{ '2': 40 / fontSizeRelative + 'rem' },
					{ '3': 30 / fontSizeRelative + 'rem' },
					{ '4': 24 / fontSizeRelative + 'rem' },
					{ '5': 20 / fontSizeRelative + 'rem' },
					{ '6': 18 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'font-size-text',
				selector: "text",
				options: [
					{ '1': 16 / fontSizeRelative + 'rem' },
					{ '2': 15 / fontSizeRelative + 'rem' },
					{ '3': 14 / fontSizeRelative + 'rem' },
					{ '4': 12 / fontSizeRelative + 'rem' },
					{ '5': 10 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'colors',
				selector: "cf",
				options: [
					{
						name: 'dark',
						selector: "0",
						options: [
							{ '0': '#000' },
							{ '1': '#4F4F4F' },
							{ '2': '#252F40' },
						]
					},
					{
						name: 'white',
						selector: "1",
						options: [
							{ '0': '#fff' },
						]
					},
					{
						name: 'gray',
						selector: "2",
						options: [
							{ '0': '#828282' },
							{ '1': 'rgba(130, 130, 130, 0.3)' },
							{ '2': 'rgba(90, 90, 90, 0.4)' },
							{ '3': '#979797' },
							{ '4': '#BDBDBD' },
							{ '5': 'rgba(189, 189, 189, 0.4)' },
							{ '6': 'rgba(17, 17, 17, 0.48)' },
							{ '7': 'rgba(189, 189, 189, 0.5)'},
						]
					},
					{
						name: 'green',
						selector: "3",
						options: [
							{ '0': '#37A48C' },
							{ '1': 'rgba(55, 164, 140, 0.8)' },
							{ '2': '#6FCF97' },
							{ '3': '#258973' },
							{ '4': '#439F6E' }
						]
					},
					{
						name: 'blue',
						selector: "4",
						options: [
							{ '1': '#C9C8D3' }
						]
					},
					{
						name: 'red',
						selector: "5",
						options: [
							{'0': '#F93232'},
							{'1': '#E77373'}
						]
					},
				]
			},
			{
				name: 'background-color',
				selector: "bgc",
				options: [
					{
						name: 'white',
						selector: "0",
						options: [
							{'0': '#fff'},
						]
					},
					{
						name: 'gray',
						selector: "1",
						options: [
							{'0': '#ECECEC'},
							{'1': '#F0F0F0'},
							{'2': '#FFF5F5'},
							{'3': '#F1FFF4'},
							{'4': '#4F4F4F'},
							{'5': 'rgba(79, 79, 79, 0.6)'},
							{'6': '#FCFCFC'},
						]
					},
					{
						name: 'beige',
						selector: "2",
						options: [
							{'0': '#F7F7F7'},
							{'1': 'rgba(247, 247, 247, 0.8)'},
							{'2': 'rgba(247, 247, 247, 0.5)'}
						]
					},
					{
						name: 'red',
						selector: "3",
						options: [
							{'0': '#E77373'},
							{'1': 'rgba(231, 115, 115, 0.15)'},
						]
					},
					{
						name: 'green',
						selector: "4",
						options: [
							{'0': '#37A48C'},
							{'1': '#258973'},
							{'2': '#ECF4EA'}
						]
					},
					{
						name: 'dark',
						selector: "5",
						options: [
							{'0': '#252F40'},
						]
					},
				]
			},
			{
				name: 'border-color',
				selector: "bdc",
				options: [
					{
						name: 'white',
						selector: "0",
						options: [
							{'0': '#FFFFFF'},
							{'1': 'rgba(255, 255, 255, 0.5)'},
							{'2': 'rgba(255, 255, 255, 0.3)'}
						]
					},
					{
						name: 'gray',
						selector: "1",
						options: [
							{'0': '#CECECE'},
							{'1': 'rgba(130, 130, 130, 0.8)'},
							{'2': 'rgba(189, 189, 189, 0.8)'},
							{'3': 'rgba(189, 189, 189, 0.5)'},
							{'4': 'rgba(189, 189, 189, 0.3)'},
							{'5': '#BDBDBD'},
						]
					},
					{
						name: 'green',
						selector: "2",
						options: [
							{'0': '#258973'},
							{'1': '#C0ECD4'},
							{'2': '#37A48C'}
						]
					},
					{
						name: 'red',
						selector: "3",
						options: [
							{'0': '#FFD8D8'},
							{'1': '#E77373'}
						]
					},
				]
			},
		]
	}
];