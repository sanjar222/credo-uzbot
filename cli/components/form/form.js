(function() {
  var forms = document.querySelectorAll('.needs-validation');  
  validation = Array.prototype.filter.call(forms, function(form) {
    
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
})();