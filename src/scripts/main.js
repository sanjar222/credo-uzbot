document.addEventListener("DOMContentLoaded", () => {
	svg4everybody();
	var forms = document.querySelectorAll('.needs-validation');
	var validation = Array.prototype.filter.call(forms, function (form) {
		form.addEventListener('submit', function (event) {
			if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated');
		})
	});

	$('[data-element="select"]').each(function (i, el) {
		$(el).select2({});
		$(el).on('select2:select', function (event) {
			var $searchfield = $(this).parent().find('.select2-search__field');
			$searchfield.val('');
		});
		if ($(el).attr('multiple') && $(el).data('search') === false) {
			$(el).on('select2:opening select2:closing', function (event) {
				var $searchfield = $(this).parent().find('.select2-search__field');
				$searchfield.prop('disabled', true);
			});
		}
	})

	var chartElems = document.querySelectorAll('[data-element="chart"]');
	var charts = Array.prototype.filter.call(chartElems, function (elem) {
		var chart = elem.getContext('2d');
		var myChart = new Chart(chart, {
			type: 'line',
			data: {
				labels: ['Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
				datasets: [{
						label: '#1',
						data: [0, 100, 80, 150, 250, 180],
						borderWidth: 3,
						fill: false,
						borderColor: '#4f4f4f',
					},
					{
						label: '#2',
						data: [0, 80, 120, 110, 220, 110],
						borderWidth: 3,
						fill: false,
						borderColor: '#22BE9C',
					}
				]
			},
			options: {
				plugins: {
					legend: {
						display: false
					},
				}
			}
		});
	})

	$('[data-element="toggle"]').on('click', function (e) {
		var _target = $(this).data('target');
		if (_target === '#app' && !$(_target).hasClass('active')) {
			$('body').addClass('scroll-hidden')
		} else {
			$('body').removeClass('scroll-hidden')
		}
		$(_target).toggleClass('active');
	})

	let drag = document.querySelector("#products-list-group");

	new Sortable(drag, {
		animation: 150
	});

	$('.list-group__btn--base').on('click', function () {
		$('.list-group__item').toggleClass('active');
	});

	$('.list-group__btn--second').on('click', function () {
		$('.list-group__item').removeClass('active');
	});

		var datepickers = document.querySelectorAll('[data-element="datepicker"]');
			Array.prototype.filter.call(datepickers, function (elem) {
				console.log(elem);
				// console.log(datepicker);
			$(elem).datepicker({
				format: 'dd.mm.yyyy',
				container: `${$(elem).data('container')}`,
				maxViewMode: 2,
				orientation: 'bottom left'
			});
		})

	// var sortableTags = document.querySelectorAll('[data-element="sortable"]');
	// Array.prototype.filter.call(sortableTags, function (sortable) {
	// 	var instance = new Sortable(sortable, {
	// 		group: 'nested',
	// 		animation: 150,
	// 		fallbackOnBody: true,
	// 		swapThreshold: 0.65,
	// 		handle: '[data-element="handle"]',
	// 		onChange: function (ev) {
	// 			controlToggleVisibilityOnSortableChange(ev);
	// 		}
	// 	});
	// });

	@@include('./components.js')
});